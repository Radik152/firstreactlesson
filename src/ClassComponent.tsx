import React from 'react';
import style from './ClassComponent.module.scss';

interface IProps {
    data?: string;
    count?: number; // ?- означает необязательность
}
interface IState {
    click: number;
    name: string;
}
class ClassComponent extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {click:0, name:"Radik"};
        this.secondHandle = this.secondHandle.bind(this);
    }

    handleClick = () => {
        const {click} = this.state;
        this.setState({click: click +1 });
    }

    secondHandle() {
        const {click} = this.state;
        this.setState({click: click -1 });
    }

    render() {
        const {data, count} = this.props;
        const {click} = this.state;
        const {name} = this.state;
        return  (
            <>
            <h1>Привет, я классовый компонент!</h1>
            {data}
            {count}
            <br/>
            <h1>{name}</h1>
            <button 
                type="button" 
                onClick={this.handleClick} 
                style={{width:'200px'}}>
                Push me
            </button>
            <button type="button" onClick={this.secondHandle} className={style.wrapper}>Push</button>
            <h1>{click}</h1>
            {/* <h1>Привет, я классовый компонент!</h1> */}
            </>
        );
        
    }
}

export default ClassComponent;