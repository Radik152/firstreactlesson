"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var react_1 = require("react");
var ClassComponent_module_scss_1 = require("./ClassComponent.module.scss");
var ClassComponent = /** @class */ (function (_super) {
    __extends(ClassComponent, _super);
    function ClassComponent(props) {
        var _this = _super.call(this, props) || this;
        _this.handleClick = function () {
            var click = _this.state.click;
            _this.setState({ click: click + 1 });
        };
        _this.state = { click: 0, name: "Radik" };
        _this.secondHandle = _this.secondHandle.bind(_this);
        return _this;
    }
    ClassComponent.prototype.secondHandle = function () {
        var click = this.state.click;
        this.setState({ click: click - 1 });
    };
    ClassComponent.prototype.render = function () {
        var _a = this.props, data = _a.data, count = _a.count;
        var click = this.state.click;
        var name = this.state.name;
        return (react_1["default"].createElement(react_1["default"].Fragment, null,
            react_1["default"].createElement("h1", null, "\u041F\u0440\u0438\u0432\u0435\u0442, \u044F \u043A\u043B\u0430\u0441\u0441\u043E\u0432\u044B\u0439 \u043A\u043E\u043C\u043F\u043E\u043D\u0435\u043D\u0442!"),
            data,
            count,
            react_1["default"].createElement("br", null),
            react_1["default"].createElement("h1", null, name),
            react_1["default"].createElement("button", { type: "button", onClick: this.handleClick, style: { width: '200px' } }, "Push me"),
            react_1["default"].createElement("button", { type: "button", onClick: this.secondHandle, className: ClassComponent_module_scss_1["default"].wrapper }, "Push"),
            react_1["default"].createElement("h1", null, click)));
    };
    return ClassComponent;
}(react_1["default"].Component));
exports["default"] = ClassComponent;
